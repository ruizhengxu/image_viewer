#ifndef IMAGE_VIEWER__HPP
#define IMAGE_VIEWER__HPP

#include <rclcpp/rclcpp.hpp>
#include <image_transport/image_transport.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui.hpp>


class ImageViewer : public rclcpp::Node
{
public:
    ImageViewer()
    : rclcpp::Node("img_viewer")
    {   
        this->declare_parameter("save", false);
        save = this->get_parameter("save").get_parameter_value().get<bool>();
        counter = 0;
        throttle = 1;

        // ROS subscribers
        auto QoS_camera = rclcpp::QoS(1);
        QoS_camera.keep_last(1).best_effort();
        
        sub_camera_ = this->create_subscription<sensor_msgs::msg::Image>(
            "~/image", QoS_camera, std::bind(&ImageViewer::sub_camera_cb, this, std::placeholders::_1));

        sub_camera_compressed_ = this->create_subscription<sensor_msgs::msg::CompressedImage>(
            "~/image/compressed", QoS_camera, std::bind(&ImageViewer::sub_camera_compressed_cb, this, std::placeholders::_1));

        RCLCPP_INFO(this->get_logger(), "Image Viewer Node launched -----");
    }

protected:
    bool save;
    int throttle;
    int counter;

    // ROS subscribers
    rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr sub_camera_;
    rclcpp::Subscription<sensor_msgs::msg::CompressedImage>::SharedPtr sub_camera_compressed_;
    
    void sub_camera_cb(const sensor_msgs::msg::Image::ConstSharedPtr msg);
    void sub_camera_compressed_cb(const sensor_msgs::msg::CompressedImage::ConstSharedPtr msg);
};

#endif