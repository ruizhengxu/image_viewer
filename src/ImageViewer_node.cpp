#include <image_viewer/ImageViewer.hpp>

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ImageViewer>());
    rclcpp::shutdown();
    return 0;
}