#include <image_viewer/ImageViewer.hpp>

//-----------------------------------------------------------------------------
void ImageViewer::sub_camera_cb(const sensor_msgs::msg::Image::ConstSharedPtr msg)
{
    RCLCPP_DEBUG(this->get_logger(), "Received image");

    // Convert ROS2 image to OpenCV
    auto cv_ptr_in = cv_bridge::toCvCopy(msg,"bgr8");
    cv::namedWindow("img", cv::WINDOW_NORMAL);
    cv::resizeWindow("img",600,600);
    cv::imshow("img", cv_ptr_in->image);
    cv::waitKey(1);
    
    if (save)
    {
        throttle += 1;
        if (throttle == 10)
        {
            std::string img_name = std::to_string(counter) + ".png";
            cv::imwrite(img_name, cv_ptr_in->image);
            counter++;
            throttle = 1;
        }
    } 
}

//-----------------------------------------------------------------------------
void ImageViewer::sub_camera_compressed_cb(const sensor_msgs::msg::CompressedImage::ConstSharedPtr msg)
{
    RCLCPP_DEBUG(this->get_logger(), "Received image compressed");

    // Convert ROS2 image to OpenCV
    auto cv_ptr_in = cv_bridge::toCvCopy(msg,"bgr8");
    cv::namedWindow("compressed_img", cv::WINDOW_NORMAL);
    cv::resizeWindow("compressed_img",600,600);
    cv::imshow("compressed_img", cv_ptr_in->image);
    cv::waitKey(1);

    if (save)
    {
        throttle += 1;
        if (throttle == 10)
        {
            std::string img_name = std::to_string(counter) + "_compressed.png";
            cv::imwrite(img_name, cv_ptr_in->image);
            counter++;
            throttle = 1;
        }
    }
}
