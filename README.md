# Image_Viewer

This node does not publish any message, it displays received images with opencv.

## Dependencies

- `cv_bridge`
- `sensor_msgs`

## Build

`colcon build --packages-select image_viewer`

## Running Image Viewer Node

`ros2 run image_viewer img_viewer_node --ros-args -r /img_viewer/image:=/your/image/topic -r /img_viewer/image/compressed:=/your/compressed_image/topic`